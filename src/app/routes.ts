import {Routes} from "@angular/router";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {AppComponent} from "./app.component";

export const ROUTES: Routes=[
  {path:"**",component:PageNotFoundComponent},
];
